#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 19:08:12 2021

@author: nikolai
"""

#function to accept list of texts and transform it to binary matrix
from keras.preprocessing.text import Tokenizer

def token_machine(documents):
    t = Tokenizer()#initialize Tokenizer
    t.fit_on_texts(documents)#fit text 
    
    #print various info regarding our document
    print(f'word counds: {t.word_counts}')
    print(f'document count: {t.document_count}')
    print(f'word_index: {t.word_index}')
    print(f'word_docs: {t.word_docs}')
    
    encoded = t.texts_to_matrix(documents, mode = 'count')#turn list to numpy matri
    
    return encoded




