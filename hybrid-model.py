#!/usr/bin/env python
# coding: utf-8

# ## imports
# @author: nikolai

import tensorflow as tf
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt 
from tensorflow.keras import metrics
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Activation, Dense, MaxPool1D, Flatten
from tensorflow.keras.layers import LSTM, Conv1D, RepeatVector, BatchNormalization
from tensorflow.keras.layers import Dropout, Input, concatenate, GlobalAveragePooling1D
from util import window, converter, creator, movinAvg, modifier, company, companies, constructor
# -------------------------------------------------------------------------

gpu_devices = tf.config.experimental.list_physical_devices("GPU")
for device in gpu_devices:
    tf.config.experimental.set_memory_growth(device, True)
                                         
#df_target.insert(4, 'movAvgs', movAvgs, True) # insert moving averages to final dataFrame
#reshape 1D arrays to 3D arrays to feed in the model 
#xtrain = tf.reshape(xtrain, (xtrain.shape[0], xtrain.shape[1], 4))
#xtest = tf.reshape(xtest, (xtest.shape[0], xtest.shape[1], 4))
# -------------------------------------------------------------------------

def functional_cnn(steps,
                   features,
                   ):
    
    #define inputs and their shape
    inputShape = (steps, features)
    inputs = Input(shape=inputShape)
    #create CNN
    crypto = Conv1D(filters = 100,
                kernel_size = 2,
                activation = 'relu',
                input_shape = inputShape
                )(inputs)
    crypto = MaxPool1D(pool_size = 2)(crypto)    
    crypto = GlobalAveragePooling1D()(crypto)
    crypto = Flatten()(crypto)
    
    #construct the model
    model = Model(inputs, crypto)
    
    #return the model  
    return model
# -------------------------------------------------------------------------

def functional_LSTM(steps,
                    features
                    ):
    
    #define inputs and their shape
    inputShape = (steps, features)
    inputs = Input(shape=inputShape)
    
    #create LSTM
    crypto = LSTM(units = 500,
                  return_sequences = True,
                  activation = 'tanh',
                  input_shape = inputShape
                  )(inputs)
    crypto = Flatten()(crypto)
    
    #construct the model
    model = Model(inputs, crypto)
    
    #return the model  
    return model
# -------------------------------------------------------------------------

def functional_Dense(model_a,
                     model_b
                     ):        
    
    #define inputs and their shape
    input_combined = concatenate([model_a.output, model_b.output])
    
    # build Dense
    x = Dense(64, activation = 'relu')(input_combined)  

    x = Dense(1, activation = 'linear')(x)
    
    #construct the model
    model = Model(inputs = [model_a.input, model_b.input], outputs = x)
    
    #return the model
    return model
# -------------------------------------------------------------------------

xtrain, ytrain, xval, yval, xtest, ytest, SCs = constructor()
    
cryptoniak_cnn = functional_cnn(steps = xtrain[0].shape[1],
                                features = xtrain[1].shape[2])
cryptoniak_LSTM = functional_LSTM(steps = xtrain[0].shape[1],
                                  features = xtrain[1].shape[2])
cryptoniak_combined = functional_Dense(cryptoniak_cnn, cryptoniak_LSTM)

cryptoniak_combined.compile('RMSprop', loss='binary_crossentropy') 

history = cryptoniak_combined.fit(x = [xtrain[0],
                                       xtrain[1],
                                       xtrain[2],
                                       xtrain[3]],
                                  y = ytrain[0],
                                  epochs = 50,
                                  batch_size = 8,
                                  validation_data = ([xval[0],
                                                      xval[1],
                                                      xval[2],
                                                      xval[3]], 
                                                      yval[0])
                                  )
# -------------------------------------------------------------------------

# Make predictions on test set
pred = cryptoniak_combined.predict([xtest[0],
                                    xtest[1], 
                                    xtest[2], 
                                    xtest[3]])

model_predicted = []
for i in pred:
    model_predicted.append(float(i))

close = []
for i in ytest[0]:
    close.append(i)

df_predicted = pd.DataFrame(model_predicted, columns=['predictions'])
df_predicted['predictions'] = model_predicted 
df_predicted['close'] = close[:]

# #### plots

plt.figure(figsize=(25, 15))
plt.plot(df_predicted['predictions'], label='predictions')
plt.plot(df_predicted['close'], label='targets')
plt.legend()
plt.title(company)
plt.show()
# -------------------------------------------------------------------------

# next days predictions
scaler = SCs[0]
temp = np.append(xtest[0][-1][-1], pred[-1]).reshape(1,-1)
daily_pred = scaler.inverse_transform(temp)


print(f"\n today's prediction for {company} places tomorrow's price at approximately: {daily_pred[0][-1]}")




