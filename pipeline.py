#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 14:04:37 2020

@author: nikolai
"""


from tensorflow import keras
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt 
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
import plotly.graph_objects as go
import operator
import requests
import re, os

API_URL = input('enter your API url: ')
API_KEY = input('enter your API key: ')
company = input('enter the company you are interested in : ')

def caller(API_URL, API_KEY, company):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v3/get-historical-data"
    querystring = {
        "symbol": company,
        "region":"US"
        }
    headers = {
        'x-rapidapi-key': API_KEY,
        'x-rapidapi-host': API_URL
        }
    response = requests.request("GET", url, headers=headers, params=querystring)
    plain = response.text # save to plain text (str) variable
    
    #write to file for future use
    f = open("./plain.txt", "w")
    f.write(plain)
    f.close()
    
    
    fr = open('./plain.txt', 'r')
    file = fr.read()
    fr.close
    
    return file

#function to bring messy API call data to a structured format
def modifier(file):
    mod1 = file.split('{') # create a list separated by '{'
    
    cleanList = []
    for j in range(0, len(mod1)):
        cleanList.append(mod1[j]) # create new list with omitted fuss
    
    mod2 = []
    for j in range(len(cleanList)):
        mod2.append(cleanList[j].split(',')) # separate values correctly
        del mod2[j][-1] # remove emptiness from rows
    
    mod2 = list(filter(None, mod2)) # remove empty elements

    return mod2

#function to cleanup and omit useless data
def remover(modified):
    
    copy = []
    for i in range(0, len(modified)):
        copyRow = []
        if '"date"' in modified[i][0].split(':'): # append only if line represents date 
            if '"close"' in modified[i][4].split(':'): # append only if it eventually represents a stock
                for j in range(0, 7):
                    temp = modified[i][j].split(':')[1] # keep only numbers
                    copyRow.append(temp)
                
                copyRow[6] = copyRow[6].replace('}','')
                copy.append(copyRow)
    return copy

#write2csv
def creator(modified):
    path = ('./datasets/dailyAcquired')
    existing = os.listdir(path)
    
    if (f'{company}.csv' in existing): #check if a corresponding dataset already exists
        dframe = pd.read_csv('./datasets/dailyAcquired/{}.csv'.format(company))
        dframe = dframe.drop(columns='Unnamed: 0')
        dframe = dframe.dropna()
        
        add_row = {'date':modified[0][0],
                    'open':modified[0][1],
                    'high':modified[0][2],
                    'low':modified[0][3],
                    'close':modified[0][4],
                    'volume':modified[0][5],
                    'adjclose':modified[0][6]}
        
        dframe = dframe.append(add_row, ignore_index = True)
        dframe.to_csv('./datasets/dailyAcquired/{}.csv'.format(company))
        
    else:#if there's not a previous dataset, create a new one         
        df = pd.DataFrame(data = modified, columns = ['date', 'open', 'high', 'low', 'close', 'volume', 'adjclose'])
        df = df.sort_values('date')
        df = df.dropna()

        df.to_csv('./datasets/dailyAcquired/{}.csv'.format(company))

#function calls 
file = caller(API_URL, API_KEY, company)
modified = modifier(file)
modified = remover(modified)

# call the writer
creator(modified)
