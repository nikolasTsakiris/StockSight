#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 16:10:34 2021

@author: nikolai
"""

#imports 
from datetime import date
import pandas as pd
from numpy import array
import matplotlib.pyplot as plt 
from sklearn.preprocessing import MinMaxScaler

#import data
companies = ['BTC-USD',
             'XMR-USD',
             'ETH-USD',
             'DOGE-USD',
             'ADA-USD',
             'EGLD-USD',
             'AVAX-USD'
            ]

company = input("Please give CodeName of your preferred company: ")

#function to shift the target values by a specified amount (int)
def window(data, shift_value: int):
    data['target'] = data[['Close']].shift(-shift_value)
    
#function for epoch conversion
def converter(epochs):
    stamps = [] #create an empty list
    
    for i in range(len(epochs)):
        stamp = epochs.iloc[i].values #get the timestamp
        stamp = date.fromtimestamp(stamp) #convert the timestamp 
        stamps.append(stamp) #append the converted timestamp
    
    return stamps

#function for generating DataFrames in a fixed manner
def creator(company):
    
    df = pd.read_csv('./datasets/yahoo/{}.csv'.format(company))
    #df = df.drop(columns='Unnamed: 0')
    #df = df.dropna()
            
    frame = pd.DataFrame(df, columns = ['Close',
                                        'Volume',
                                        'High',
                                        'Low',
                                        ])    

    return frame

#function for calculating moving averages
def movinAvg(df, percentage):
    frame = int(len(df) * percentage) # set the frame value
    
    temp = df['Close'].rolling(frame).mean() # set a window equal to pre-defined frame
    dfAvgs = pd.DataFrame(temp) # convert to dataframe
    
    return dfAvgs

#function for generating time series data in a sequential manner
def modifier(data, steps):
    X, y = list(), list() # list initiation
    for i in range(len(data)):
        endIDX = i + 3 # define the step gap
        if endIDX > len(data): # if end index greater than total length stop
            break
        
        """
        Below, we define seqx as the features and seqy as the target values.
        For example, sequence [1, 2, 3, 4] for step gap 3 will produce 
        [1, 2, 3] [4] for seqx and seqy respectively.
        """
        seqx, seqy = data[i:endIDX, 0:4], data[endIDX-1, -1] 
        X.append(seqx)
        y.append(seqy)
        
    return array(X), array(y)

#function to construct training, validation and testing sets
def constructor():
    
    names = [] #list to contain all available datasets
    movAvgs = []#moving averages for all available datasets
    SCs = []#separate scalers, one for each dataset
    xtrain, ytrain, xval, yval, xtest, ytest = list(), list(), list(), list(), list(), list()
    
    for i in range(len(companies)): # loop through all available datasets
        names.append(creator(companies[i]))
        window(names[i], 1)
        movAvgs.append(movinAvg(names[i], 0.03)) # calculate moving averages
    
        #fill na
        names[i] = names[i].fillna(method = 'backfill')
        # define data for each crypto
        data = names[i].values
    
        #define split percentages
        split = int(len(data) * 0.65)
        val_split = int(len(data) * 0.25) + split
        
        #split data
        training = data[:split]
        validation = data[split:val_split]
        testing = data[val_split:-1]
    
        #scaling
        sc = MinMaxScaler(feature_range=(0,1))
        SCs.append(sc)
        train_scaled = sc.fit_transform(training)
        val_scaled = sc.transform(validation)
        test_scaled = sc.transform(testing)
        
        #create our sets
        a, b = modifier(train_scaled, 7)
        c, d = modifier(val_scaled, 7)
        e, f = modifier(test_scaled, 7)
        xtrain.append(a)
        ytrain.append(b)
        xval.append(c)
        yval.append(d)
        xtest.append(e)
        ytest.append(f)
        
    return xtrain, ytrain, xval, yval, xtest, ytest, SCs

# PLOTS
def show(data, label):
    plt.figure(figsize=(10,5))
    plt.plot(data)
    plt.title(label)
    plt.grid()
    
















